require_relative 'base'
require_relative 'labor'

# migrate LaborUser to Labor::User
def migration_1
  LaborUser = Struct.new(:id, :name, :labor, :chat_id, :notified_of_max_labor)

  file_name "data/labor/users.data"
  old_users = load_file file_name, []
  new_users = old_users.map { |u| Labor::User.new u.id u.name u.labor u.chat_id u.notified_of_max_labor }

  write_file file_name, new_users
end

migration_1
