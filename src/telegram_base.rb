class BaseListener
  def initialize(bot, message)
    @bot = bot
    @chat = message.chat
    @from = message.from
    @text = message.text

    if @text&.start_with? command
      @args = @text.delete_prefix(command).split
      @first_two_args = "#{@args[0]} #{@args[1]}"
      fire_command
    end
  end

  def reply(text, markup: nil)
    _reply(@bot, @chat.id, text, markup: markup)
  end

  def reply_with_options(text, options)
    reply text, markup: Types::ReplyKeyboardMarkup.new(keyboard: options, one_time_keyboard: true)
  end
end

def _reply(bot, chat_id, text, markup: nil)
  if markup
    bot.api.send_message(chat_id: chat_id, text: text, reply_markup: markup)
  else
    bot.api.send_message(chat_id: chat_id, text: text)
  end
end
