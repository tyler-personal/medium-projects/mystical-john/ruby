require 'telegram/bot'

require_relative 'base'
require_relative 'labor'
require_relative 'trade_pack'

include Telegram::Bot

# TOKEN = "1053308154:AAE00bSrKsQVC_ETWdURcMOrfkrKZ7939Gc"
# TOKEN = "1017165725:AAHfmCgrT99lJ5bvzlq5VJwGQin7HYpGBoE"
TOKEN = "1053308154:AAE00bSrKsQVC_ETWdURcMOrfkrKZ7939Gc"

class Main
  def initialize(bot)
    @bot = bot
    @bot.listen do |message|
      [Labor::Listener, TradePack::Listener, TradePack::StageListener].each do |m|
        m.new @bot, message
      end
    end
  end
end

Client.run(TOKEN) do |bot|
  $bot = bot
  bot_thread = Thread.new { Main.new $bot }

  ([bot_thread] + Labor::threads).each(&:join)
end
