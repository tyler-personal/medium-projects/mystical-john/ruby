require 'telegram/bot'

require_relative 'base'

include Telegram::Bot

module TradePack
  module Quality
    Luxury = {
      high: {percentage: 30, minutes: (0..14)},
      regular: {percentage: 10, minutes: (15..29)},
      reduced: {percentage: -10, minutes: (30..59)},
      low: {percentage: -15, minutes: (60..)}
    }
    Fine = {
      high: {percentage: 15, minutes: (0..14)},
      regular: {percentage: 5, minutes: (15..29)},
      reduced: {percentage: -7, minutes: (30..179)},
      low: {percentage: -12, minutes: (180..)}
    }
    Commercial = {
      high: {percentage: 5, minutes: (0..29)},
      regular: {percentage: 2, minutes: (30..179)},
      reduced: {percentage: -5, minutes: (180..719)},
      low: {percentage: -10, minutes: (720..)}
    }
    Preserved = {
      high: {percentage: 3, minutes: (0..359)},
      regular: {percentage: 1, minutes: (360..719)},
      reduced: {percentage: -4, minutes: (720..1439)},
      low: {percentage: -8, minutes: (1440..)}
    }
  end


  HARANYA_REGIONS = []

  PRIMARY_COMPONENTS = ["Ground Grain", "Dried Flowers", "Trimmed Meat", "Chopped Produce", "Medicinal Powder"]
  SECONDARY_COMPONENTS = ["Cucumber", "Idris"]
  MODIFIERS = ["Local Speciality", "Gilda Speciality", "Speciality"]
  PRIMARY_COUNTS = [%w(150 160), %w(180 200)]
  SECONDARY_COUNTS = %w(3 5 15)
  NUMBERS = [%w(1 2 3), %w(4 5 6), %w(0)]

  NUIA_REGION_TO_SPECIALTY = {
    "Two Crowns": Quality::Luxury,
    "Cinderstone": Quality::Luxury,
    "Solzreed": Quality::Luxury,
    "Marianople": Quality::Fine,
    "Dewstone": Quality::Fine,
    "Lilyut": Quality::Fine,

    "Sanddeep": Quality::Preserved
  }


  NUIA_REGIONS = NUIA_REGION_TO_SPECIALTY.keys.group_by { |k| NUIA_REGION_TO_SPECIALTY[k] }.values.map { |a| a.map(&:to_s) }

  User = Struct.new :id, :name, :chat_id, :stage, :pack
  Pack = Struct.new :region, :modifier, :component_1, :count_1, :component_2, :count_2 do
    def full_name
      mod = modifier.nil? ? "" : " #{modifier}"
      quality = nuia_region_to_specialty[region]

      "#{region} #{quality}#{mod} Speciality"
    end
  end

  Route = Struct.new :pack, :destination, :time

  def self.user_file
    "data/tradepack/users.data"
  end

  ComponentsToCost = {}
  Packs = []
  #Packs = [
  #  Pack.new "Sanddeep", Modifier.Local, {"Ground Grain": 160, "Cucumber": 15}
  #  Pack.new "Two Crowns", nil, {"Ground Grain": 180, "Pomegranate"}
  #]

  Users = load_file TradePack.user_file, []
  class Listener < BaseListener
    def command
      "!tradepack"
    end

    def fire_command
      user = User.new @from.id, @from.first_name, @chat.id, 0, Pack.new(nil, nil, nil, nil, nil, nil)
      TradePack::Users << user
      reply_with_options("Which continent?", ["Nuia", "Haranya"])
    end
  end

  class StageListener < BaseListener
    def command
      ""
    end

    def fire_command
      return unless @user = TradePack::Users.find { |u| u.id = @from.id }

      case @user.stage
      when 0 then @user.stage = 1
      when 1 then region
      when 2 then modifier
      when 3 then component_1
      when 4 then count_1
      when 5 then component_2
      when 6 then count_2
      when 7 then check_cost
      when 8 then check_cost_response
      when 9 then cost_1
      when 10 then cost_1_only
      when 11 then cost_2
      end
    end

    private

    def region
      return unless continent = check_args("continent") { |x| ["Nuia", "Haranya"].include? x }

      # options = continent == "Nuia" ? NUIA_REGIONS : HARANYA_REGIONS
      # reply_with_options "Which region?" options
      # @user.stage = 2

      if continent == "Nuia"
        reply_with_options "Which region?", NUIA_REGIONS
        @user.stage = 2
      else
        reply "No data for that yet"
        Users.delete @user
      end
    end


    def modifier
      return unless @user.pack.region = check_args("region") do |r|
        NUIA_REGIONS.flatten.include?(r) || HARANYA_REGIONS.flatten.include?(r)
      end

      reply_with_options "Which kind of speciality?", MODIFIERS
      @user.stage = 3
    end

    def component_1
      return unless @user.pack.modifier = check_args("modifier") { |x| MODIFIERS.include? x }

      pack = Packs.find { |x| @user.pack.region == x.region and @user.pack.modifier == x.modifier }

      if pack.nil?
        reply_with_options "What's the primary component?", PRIMARY_COMPONENTS
        @user.stage = 4
      else
        @user.pack = pack
        @user.stage = 7
      end
    end

    def count_1
      return unless @user.pack.component_1 = check_args("component") { |x| PRIMARY_COMPONENTS.include? x }

      reply_with_options "How many #{@user.pack.component_1}(s)?", PRIMARY_COUNTS
      @user.stage = 5
    end

    def component_2
      return unless @user.pack.count_1 = check_args("count") { |x| x.to_i_or_nil }

      reply_with_options "What's the secondary component?", SECONDARY_COMPONENTS
      @user.stage = 6
    end

    def count_2
      return unless @user.pack.component_2 = check_args("component") { |x| SECONDARY_COMPONENTS.include? x }

      reply_with_options "How many #{@user.pack.component_2}(s)?", SECONDARY_COUNTS
      @user.stage = 7
    end

    def check_cost
      c1 = @user.pack.component_1
      c2 = @user.pack.component_2
      cost_1_exists = ComponentsToCost.key? c1
      cost_2_exists = ComponentsToCost.key? c2

      if cost_1_exists and cost_2_exists
        [["Both", "No"], [c1, c2]]
      elsif cost_1_exists
        reply "You only have #{c1} price data."
        [c1, "No"]
      elsif cost_2_exists
        reply "You only have #{c2} price data."
        [c2, "No"]
      end

      if cost_1_exists || cost_2_exists
        reply_with_options "Would you like to reuse component costs?", options
        @user.stage = 8
      else

        @user.stage = 9
      end

    end


    def check_cost_response
      return unless check_cost = check_args("check") do |x|
        ["Both", "No", @user.pack.component_1, @user.pack.component_2].include? x
      end

      case check_cost
      when "Both" then @user.stage = 12
      when "No" then @user.stage = 9
      when @user.pack.component_1 then @user.stage = 11
      when @user.pack.component_2 then @user.stage = 12
      end

    end

    def cost_1
      @user.stage = 11
    end

    def cost_1_only
      @user.stage = 13
    end

    def cost_2
      @user.stage = 13
    end

    def check_args(arg_type, &block)
      if block.call(@args[0])
        @args[0]
      elsif block.call(@first_two_args)
        @first_two_args
      else
        reply "Invalid #{arg_type}, make sure to use the custom keyboard."
        Users.delete @user
        nil
      end
    end
  end
end
