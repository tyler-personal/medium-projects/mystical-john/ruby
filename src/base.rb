def load_file(name, default)
  if File.file? name
    Marshal.load(File.binread(name))
  else
    default
  end
end

def write_file(name, data)
  File.open name, "wb" do |f|
    f.write(Marshal.dump(data))
  end
end

def execute_every(min, new_thread: true)
  execution = -> do
    loop do
      time = Time.now
      if time.min % min == 0
        yield
      end
      sleep 60
    end
  end

  if new_thread
    Thread.new { execution.call }
  else
    execution.call
  end
end

class Object
  def as
    yield self
  end

  def to_i_or_nil
    Integer(self&.sub(/^[0]+/,''), exception: false)
  end
end
