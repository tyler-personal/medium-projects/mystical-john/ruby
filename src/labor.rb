require_relative 'base'
require_relative 'telegram_base'

module Labor
  User = Struct.new(:id, :name, :start_labor, :start_time, :chat_id, :notified_of_max_labor) do
    def labor
      time = Time.now
      labor_ticks = (start_time - time).round / Labor::FIVE_MINUTES
      start_labor + (labor_ticks * 10)
    end
  end

  MAX = 5000
  IN_AN_HOUR = 120
  FIVE_MINUTES = 300

  class Listener < BaseListener
    def self.user_file
      "data/labor/users.data"
    end

    def self.users
      @@users
    end

    @@users = load_file Listener.user_file, []

    def command
      "!labor"
    end

    def fire_command
      @user = @@users.find { |u| u.id == @from.id }

      case @args[0]
      when nil then usage
      when "set" then set
      when "now" then now
      when "until" then until_labor
      end

      unless @user.nil?
        @user.chat_id = @chat.id
      end
    end

    private

    def usage
      reply "Usages:\n\t\t!labor set num\n\t\t!labor now"
    end

    def set
      labor = @args[1].to_i_or_nil

      case
      when @args[1].nil?
        reply "Usage:\n\t\t!labor set num\nNote:\n\t\tnum must be between 0 and #{Labor::MAX}"
      when labor.nil? || !(((0..Labor::MAX).to_a).include? labor)
        reply "Must enter a number between 0 and 5000"
      when @user.nil?
        reply "Labor set to #{labor}"
        @user = User.new @from.id, @from.first_name, labor, Time.now, @chat.id, false
        @@users << @user
        hours_from_labor
      else
        reply "Labor changed from #{@user.labor} to #{labor}"
        @user.start_labor = labor
        @user.start_time = Time.now
        @user.notified_of_max_labor = false
        hours_from_labor
      end
    end

    def now
      if @user.nil?
        reply "You must set your labor before checking it."
      else
        reply "Labor is #{@user.labor} (+/- 10)"
        hours_from_labor
      end
    end

    def until_labor
      labor = @args[1].to_i_or_nil

      case
      when @user.nil?
        reply "You must set your labor first."
      when labor.nil? || !(((@user.labor..Labor::MAX).to_a).include? labor)
        reply "Usage:\n\t\t!labor until num\nNote:\n\t\tnum must be between #{@user.labor} and #{Labor::MAX}"
      else
        hours_from_labor(final_amount: labor, amount: labor.to_s)
      end
    end

    def hours_from_labor(final_amount: Labor::MAX, amount: "max")
      if @user.labor == final_amount
        reply "You are at #{amount} labor!"
      else
        labor_left = final_amount - @user.labor
        time_left = labor_left / Labor::IN_AN_HOUR.to_f

        hours_left = time_left.to_i
        minutes_left = (60 * (time_left % 1)).to_i

        hour_now, min_now = Time.now.as { |t| [t.hour, t.min] }

        hour_rem, hour = (hour_now + hours_left).divmod 12
        extra_hour, min = (min_now + minutes_left).divmod 60

        final_time = "#{hour + extra_hour}:#{min} #{hour_rem.even? ? "AM" : "PM"}"

        reply "You are #{time_left.round(2)} hours from #{amount} labor\nIt will be #{final_time}"
      end
    end
  end

  def Labor.threads
    labor_update_thread = execute_every(5) do
      Listener.users.each do |user|
        time = Time.now
        reply = -> (text) { _reply $bot, user.chat_id, "#{user.name}, #{text}" }

        if user.labor > Labor::MAX
          user.labor = Labor::MAX
        end
        case user.labor
        when Labor::MAX
          unless user.notified_of_max_labor
            reply.call "you're at max labor!"
            user.notified_of_max_labor = true
          end
        when Labor::MAX - Labor::IN_AN_HOUR
          reply.call "you're an hour from max labor!"
        when Labor::MAX - (4 * Labor::IN_AN_HOUR)
          reply.call "you're 4 hours from max labor."
        when Labor::MAX - (12 * Labor::IN_AN_HOUR)
          reply.call "you're 12 hours from max labor."
        when Labor::MAX - (24 * Labor::IN_AN_HOUR)
          reply.call "you're 24 hours from max labor."
        end
      end
    end

    file_backup_thread = execute_every(10) do
      write_file Listener.user_file, Listener.users
    end

    [labor_update_thread, file_backup_thread]
  end
end
